# Learning Process

## Question 1

### What is the Feynman Technique?

The Feynman Technique is a method for learning or reviewing a concept quickly by explaining it in plain, simple language. It is a four-step process.

1. Choose a concept you want to learn about.
2. Explain the concept using simple language so that even kids can understand.
3. Review your explanation and identify the areas where you didn’t know something or where you feel your explanation is shaky.
4. If there are any areas in your explanation where you’ve used lots of technical terms or complex language, re-write these sections in simpler terms.

## Question 2

### What are the different ways to implement this technique in your learning process?

The different ways are:

1. Write the concept in simple language.
2. Give the notes to someone and ask them if they understood the concept.
3. Rewrite the lines which are not understood by the reader.

## Question 3

### Paraphrase the video in detail in your own words.

The video is about how to learn the concepts. We know that brain is a complicated structure, its operation can be simplified into two different modes i.e Focus mode and diffuse mode. In focus mode, you turn your attention to the concept. In diffuse mode, you will be in a relaxed state. so when you are learning you're going back and forth between these two modes. If you are learning something new or solving something for a long time, you need to put your mind in a relaxed state and start learning or solving the problem.

Procrastination is the act of lazing away when doing something that we'd rather not do. We feel physical pain in the part of our brain that analyzes pain. This pain is handled in two ways: either by procrastinating or by working a way through it. Procrastinating once or twice is not that big of a deal but doing this very often can make it kind of like an addiction. Pomodoro Technique is a way to handle procrastination where a timer is set for 25 minutes to focus on work. When the time is over, have some relaxed fun for a few minutes. Relaxation is an important part of the learning process.

## Question 4

### What are some of the steps that you can take to improve your learning process?

- When I am stuck somewhere I need to relax my mind and later start working on the problem.
- Use Pomodoro Technique when I'm feeling lazy to do some work.

## Question 5

### Your key takeaways from the video? Paraphrase your understanding.

- Decide exactly what skill we want to develop and break it down into smaller pieces. The more we break apart the skill, we can decide which all parts of the skill are useful for us. Develop these parts first.
- Get a few resources on the skill that we want to develop and practice just enough to self-correct the mistakes in practice
- Remove all distractions that hinder us to develop the skill.
- Don't get frustrated while learning the skill.

## Question 6

### What are some of the steps that you can while approaching a new topic?

- Break the skill into smaller parts and practice them one by one.
- Collect good resources for the skill.
- Remove all distractions.
- Practice the skill daily for a month.
