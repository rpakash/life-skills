# Scaling

Scaling is a process of increasing the throughput of a server. Scaling alters the size of a server. In the scaling process, we either compress or expand the server resource based on the requirement. Scaling can be done by adding resources to meet the lower expectation in the existing server, or by provisioning a new server to the existing one, or both. When the load on a server increases, the performance degrades, increasing the server response time. The throughput of a server can be increased either by vertical scaling, horizontal scaling, or both.

<img alt="verical vs horizontal scaling" src="https://www.cloudzero.com/hubfs/blog/horizontal-vs-vertical-scaling.webp" width=1000 height=600 position="center">

## Vertical Scaling

The process of adding more power (Eg: CPU, RAM, Disk) to the existing server, basically adding more resources, is called Vertical scaling. Here you don't need to change your current code. Vertical scaling is limited to the maximum capacity of the server.

As shown in the above figure, when the existing server fails to meet the expected performance, the expected performance can be reached just by adding resources is called vertical scaling.

Vertical scaling is most commonly used in applications of middle-range. Vertical scaling is not only easy but also cheaper than horizontal scaling.

## Horizontal Scaling

The process of adding a server to the existing server pool is called Horizontal scaling. In horizontal scaling, your application will be divided into multiple microservices (Eg: web server, database).

As shown in the above figure, when the existing server fails to meet the expected performance, and the expected performance cannot be met just by adding resources, then we need to add a new server, this is called horizontal scaling.

Horizontal scaling needs more time to implement than vertical scaling, and the cost is higher than vertical scaling, therefor this method is used in medium to large applications.

In Horizontal scaling, a load balancer is used, whTich distributes the load among server pools.

### Load Balancer

<img src="https://www.f5.com/content/dam/f5-com/page-assets-en/home-en/resources/glossary/what%20is%20load%20balancing.png" alt="load balancer"/>

A load balancer is a device that acts as a reverse proxy and distributes network traffic across a number of servers. A load balancer is used to increase the performance and reliability of applications. They improve the overall performance of applications by spreading the load evenly across the servers, ensuring no single server bears too much load. Load balancing will improve application responsiveness and improves the availability of applications and website for users.

<!-- ![Without load balancer](without_load_balancing_diagram.png) -->
<img src="https://www.cloudflare.com/resources/images/slt3lc6tev37/25mxQ7enQpLxkGkPA65ThD/dedd9667bc99943c8f224b914307316b/without_load_balancing_diagram.png" width=600 height=500 alt="without load balancer"/>

When there is no load balancer (as shown in the above figure), the traffic is unevenly distributed. Here server-1 is overloaded while other servers are idle, increasing the response time for the clients connected to server 1.

<img src="https://www.cloudflare.com/resources/images/slt3lc6tev37/5XVUMMchWZhrGjDrVk6pU0/40234e77d9f6c0e6cdd8641f26cf9e3c/with_load_balancing_diagram.png" width=600 height=500 alt="with load balancer"/>

When there is a load balancer (as shown in the above figure), the traffic is distributed evenly, based on the given criteria and algorithm. Here all the servers are serving the requests hence there is no overloading therefore the response time is reduced.

Load balancers can be classified into two categories:

1. Layer 4:
   Layer 4 load balancers work at the transport layer. Layer 4 load balancer directs traffic based upon the data found in network and transport layer protocols (IP, TCP, UDP). For example, based on the IP address of the client, the load balancer can distribute the load to the servers.
2. Layer 7:
   Layer 7 load balancer works at the application layer, the top of the OSI model. The Layer 7 load balancer directs traffic based on the data found in the application layer protocols such as HTTP. This allows routing decisions based on attributes like HTTP header, uniform resource identifier, SSL session ID, and HTML form data.

#### Load balancing algorithm

The method used to distribute incoming traffic to the servers placed behind the load balancer is called the load balancing algorithm. The commonly used load balancing algorithms are:

- ##### Round-Robin

  Round robin is a simple load-balancing algorithm. Here the load balancer maintains the list of available servers and directs the traffic in a cyclic manner.

  <img src="https://iq.opengenus.org/content/images/2020/07/roundrobinmodified.png" alt="round-robin" width=600 height=500>

  As shown in the above figure, there are three servers A, B, and C, and the requests from clients 1, 2, 3, 4, and 5 reach the load balancer. The load balancer will forward the request from client-1 to server A, client-2 to server B, and client-3 to server C. Now the load balancer will start from server A then it sends client-4 requests to server A and client-5 requests to server B, and so on.

- ##### Weighted Round Robin

  The Weighted round-robin algorithm is the advanced version of the round-robin algorithm. Here each server is allocated a score based on the specification of the server. Higher the specification higher the request it can serve. The load balancer distributes the traffic based on the weighted score of the servers.

  <img src="https://iq.opengenus.org/content/images/2020/07/loadbalancerwightedroundrobin.png" alt="weighted round robin" width=600 height=500>

  From the above figure, let's assume that server A can handle 3 requests per second, server B can handle 2 requests per second, and server C can handle 1 request per second. So the load balancer assigns the weighted score for servers A, B, and C as 3, 2, and 1 respectively. Now request 1,2,3 are sent to server A, then request 4 and 5 to server B, and request 6 to server C. After that, all other requests will be served in a round-robin fashion.

- ##### Least Connections

  This algorithm takes into account the current server load and checks which servers have the least number of open connections, then sends the requests to those servers. This technique is most appropriate for incoming requests that have varying connection times.

- ##### Least Response Time Method
  In the least response time algorithm, the response time of the servers is taken into the account and the request is forwarded to the server that has the least response time.

* ##### Url Hash Method
  In this method, a unique hash is obtained by combining the client IP address and server IP address, and the request is directed to the particular server based on the unique hash. This algorithm is used when the client needs to reconnect to the same server when the connection is disconnected.

## References

- https://www.cloudzero.com/blog/horizontal-vs-vertical-scaling
- https://www.f5.com/services/resources/glossary/load-balancer
- https://www.cloudflare.com/en-gb/learning/performance/what-is-load-balancing/
- https://iq.opengenus.org/load-balancing-algorithms/
- https://www.cloudflare.com/en-gb/learning/performance/types-of-load-balancing-algorithms/
- https://www.enjoyalgorithms.com/blog/types-of-load-balancing-algorithms
