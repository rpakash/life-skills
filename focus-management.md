# Focus management

## Question 1

### What is Deep Work?

According to the author, Deep work is focusing without distraction when working on a demanding task, it's like having a hard focus on the task for hours.

## Question 2

### Paraphrase all the ideas in the above videos and this one in detail.

- In the first video, the author says, the optimal duration for deep work is 1 hour or sometimes even 90 min, so that we can get at least 40-45 min of deep focus on the task.

- In the second video, the author says, having the Deadline reduces the frequency of taking a break and Deadlines serve as a motivation to work, as one looks to finish the work before the deadline.

- The third video is about how deep work can help us to complete difficult tasks. The intense focus causes the myelin to develop in relevant areas of the brain which allows the brain to fire faster and cleaner. In the current world, deep work is becoming valuable and rear. The three deep work strategies are:
  1. Schedule your distraction
  2. Have a deep work regularly
  3. Get valuable sleep

## Question 3

### How can you implement the principles in your day-to-day life?

- Have a deadline for the task
- Schedule distractions or breaks.
- Deep work regularly and make it a habit.
- Get adequete sleep.

## Question 4

### Your key takeaways from the video

- Social media makes us addicted to them and consumes our valuable time.
- Social media collects our private data.
- Social media reduces our concentration power.
- The more you use social media the more likely you are to feel lonely, isolated, or depressed.
