# Listening and Assertive Communication

## What are the steps/strategies to do Active Listening?

- Avoid getting distracted by your thoughts
- Face the speaker and have eye contact
- Do not interrupt the speaker
- Use door openers phrases like "tell me more", etc
- Show that you are listening with body language
- Take appropriate notes during an important meeting
- Listen to non-verbal cues too

## According to Fisher's model, what are the key points of Reflective Listening?

- Focus on the conversation by removing any kind of distraction
- Accept the speaker's perspective without necessarily agreeing with it, this encourages the speaker to speak freely
- Reflect the mood of the speaker, reflect the emotional state with words and nonverbal communication
- Summarize what the speaker said, using the speaker's own words, thereby reflecting the essential concept of the speaker
- Respond to the speaker's specific point without moving to other subjects
- Repeat the procedure for each subject, and switch the roles of speaker and listener, if necessary
- During the reflective listening approach, both speaker and the listener use the technique of thoughtful silence, rather than engaging in idle chatter

## What are the obstacles in your listening process?

I get distracted in my own thoughts if the person is speaking for a long time. If the speaker is saying something that I disagree with, I tend to interrupt the conversation.

## What can you do to improve your listening?

- Active listening to the speaker by avoiding distraction
- Do not interrupt the speaker till the person stops talking

## When do you switch to a Passive communication style in your day-to-day life?

In my day-to-day life, I switch to Passive communication whenever I am having a conversation with higher authorities such as mentors, supervisors, police, etc

## When do you switch to Aggressive communication styles in your day-to-day life?

In my day-to-day life, I will have Aggressive communication only with close people like Family, Close friends, etc

## When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day-to-day life?

In my day-to-day life, I will have Passive Aggressive communication only with close colleagues and friends

## How can you make your communication assertive?

- When having a conversation with someone learn to recognize and name the feelings that arise
- Learn to recognize needs that you want to address to the person that you are conversing with
- Test your communication skills by practicing them with people that you feel comfortable with
- Be aware of the body language to prevent being aggressive
- Don't wait to be assertive with someone when the need arises
