# Tiny Habits

## Question 1

### Your takeaways from the video (Minimum 5 points)

- Don't depend only on motivation as it does not helps to change his/her long-term habits.
- One should make a note of what needs to change in their life and start with tiny habits daily.
- To create a habit, one requires motivation, ability, and trigger.
- Tiny habits should be gradually increased to convert them into long-term behavior.
- Tiny habits require little willpower so that it becomes easy to make it as a behavior.
- Habits can trigger old behavior, so try pairing a tiny habit with an already existing one.

## Question 2

### Your takeaways from the video in as much detail as possible

The universal formula for human behavior is B=MAP, where M is motivation, A is ability and P is prompt.
The solution for developing or having a consistent behavior is the Tiny Habits method, and it involves 3 parts:

- Shrink the behavior: Shrink the behavior to the tiniest possible habit so that only little motivation is required to do it. Find a behavior that should be easily completed in 30 sec.

- Identify an action prompt: External, internal, and action prompts are the three types of prompts. External and internal triggers are easy to ignore and, are disruptive and demotivating. Use action prompt with regular behavior to trigger a new behavior.

- Grow habits with some shine: Celebrate every win even if it is tiny, this will increases the confidence and motivation to make progress.

## Question 3

### How can you use B = MAP to make making new habits easier?

Start with a small habit so that it requires less motivation and pair the habit with an action prompt that triggers new behavior.

## Question 4

### Why it is important to "Shine" or Celebrate after each successful completion of a habit?

It increases our confidence and motivation so that we can make good progress.

## Question 5

### Your takeaways from the video (Minimum 5 points)

- Time plays an important role in making a habit, so give yourself some time.
- Optimize for starting line not the finish line.
- Don't break the chain of tiny habits.
- Create an environment that supports your habit.
- If the good habit takes 2 minutes, do it now.
- Set a barrier for bad habits.

## Question 6

### Write about the book's perspective on habit formation from the lens of identity, processes, and outcomes?

- Don't focus on the goal, focus on the journey that makes the goal easier to achieve.
- The ultimate form of intrinsic motivation is when a habit becomes part of our identity
- Solving problems based on outcomes or results only solves them temporarily

## Question 7

### Write about the book's perspective on how to make a good habit easier?

- The habit-forming process is divided into four stages:
  1. Cue: Trigger the brain to initiate the action by giving some information about the reward
  2. Craving: Increase the motivation for the reward.
  3. Response: The auction or habit that we perform to get the reward.
  4. Reward: The final stage, which gives some satisfaction.

## Question 8

### Write about the book's perspective on making a bad habit more difficult?

- Make the cues invisible and harder to reach
- Increase the steps to indulge in the bad behavior
- Make the behavior immediately unsatisfying

## Question 9

### Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

I will exercise daily, to make it easier I will go to the gym early morning, and I will reward myself by having a good breakfast.

## Question 10

### Pick one habit that you would like to eliminate or do less of. What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

I need to stop staying up late at night. I can make it hard by waking up early morning every day.
