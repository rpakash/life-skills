# Grit and Growth Mindset

## Question 1

### Paraphrase (summarize) the video in a few lines. Use your own words.

For success, the main character is not social intelligence, good look, physical health, or IQ. It is grit. Grit is a passion and perseverance for long-term goals. If you need to succeed in life in the long term you need to develop grit. The best way to develop grit is to have a growth mindset.

## Question 2

### What are your key takeaways from the video to take action on?

Following are the takeaways from the video

- Having only IQ or intelligence doesn't make you success
- Grit helps you to become a success
- To develop grit we need to have a growth mindset

## Question 3

### Paraphrase (summarize) the video in a few lines in your own words.

A growth mindset is a belief in your capacity to learn and grow. A growth mindset creates a solid foundation for great learning. Having a growth mindset will help you learn faster. People with having growth mindset believe that skills and intelligence are grown and developed. They believe that they have control over their abilities. They believe that they can learn any skills they want. They take less time to learn skills than people with a fixed mindset. The key ingredients to a growth mindset are efforts, challenges, mistakes, and feedback. Fixed mindset people tend to avoid these ingredients. To create a growth mindset we need to focus on beliefs and focus.

## Question 4

### What are your key takeaways from the video to take action on?

The key takeaways are

- We need to have a growth mindset instead of a fixed mindset
- Having a growth mindset will make the learning process easy
- To have a growth mindset we need to focus on our beliefs and focus.
- The key things for a growth mindset are efforts, challenges, mistakes, and feedback

## Question 5

### What is the Internal Locus of Control? What is the key point in the video?

The Internal Locus of Control is essentially the degree to which you believe that you have control over your life. The key point in the video is to have an internal locus of control which is the key to staying motivated. You should have control over your own life, you are responsible for all the outcomes.

## Question 6

### Paraphrase (summarize) the video in a few lines in your own words.

The video is about how to build a growth mindset. we can develop one by believing in our ability to work something out, and believe there is a full of improvements. Start again if you failed in something. And don't get demotivated when faced with failure instead learn from them and grow.

## Question 7

### What are your key takeaways from the video to take action on?

- Believe in your ability to figure things out.
- Believe there is a lifelong room full of improvements.
- Question your assumptions that limit your potential and figure out a way to overcome them.
- Develop your life curriculum that supports your passion and dreams.
- Honor your struggles during a failure and never give up.

## Question 8

### What are one or more points that you want to take action on from the manual? (Maximum 3)

- I am 100 percent responsible for my learning.
- I should realize that confusion means there is something more to understand as mentioned in point 10.
- I will understand the users very well. I will serve them and society by writing rock-solid excellent software.
