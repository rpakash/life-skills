# Energy Management

## Question 1

### What are the activities you do that make you relax - Calm quadrant?
Watching movies, sleeping, and playing video games make me relaxed or calm.

## Question 2

### When do you find getting into the Stress quadrant?
When the project deadline is near, when having 1-1 meeting season I get stressed.

## Question 3

### How do you understand if you are in the Excitement quadrant?

When I am happy or joyous then I will be in the excitement quadrant.

## Question 4

### Paraphrase the Sleep is your Superpower video in detail.

Sleeping has a big effect on our health. The side effects of sleep deprivation are:
- Affects our learning process.
- Affects our memory power.
- Reduces our brain activity.
- Reduces natural killer cells which kills tumor cells etc.
- Makes the person look old.
- Increases cardiovascular disease and heart attacks.
- Genes are distorted causing a decrease in immunity power and an increase in cancer, or heart attacks.

## Question 5

### What are some ideas that you can implement to sleep better?

- Sleep at 10 PM and wake up at 6 PM.
- Make the sleeping room temperature which makes me sleep faster and better.

## Question 6

### Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.

This video is about how exercise affects our brain and body. The benefits of doing exercise are:
- Boosts our mood and energy.
- Helps to maintain focus and attention for long period.
- Increases long-term memory power.
- It has immediate effects on the brain.
- Increases neurotransmitters like dopamine, serotonin, and noradrenaline.
- Improves your reaction time.
- Produces new brain cells in the hippocampus that increases its volume.
- Protects your brain from neurodegenerative diseases.

## Question 7

### What are some steps you can take to exercise more?

- Walk instead of taking the bike for a short distance.
- Use stairs instead of a lift.
- Indulge in household works.