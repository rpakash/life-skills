# Prevention of Sexual Harassment

Sexual harassment includes unwelcome sexual advances, requests for sexual favors, and other verbal, visual, or physical harassment of a sexual nature that affects the working environment or creates a hosting work environment.

## What kinds of behavior cause sexual harassment?

There are three forms of sexual harassment behavior:

1. Verbal harassment
2. Visual harassment
3. Physical harassment

Sexual harassment includes:

- Staring or leering
- Unwelcome touching
- Suggestive comments or jokes
- Sexually explicit pictures or posters
- Unwanted invitations to go out on dates
- Requests for sex
- Intrusive questions about a person's private life or body
- Unnecessary familiarity, such as deliberately brushing up against a person
- Insults or taunts based on sex
- Sexually explicit physical contact
- Sexually explicit emails or SMS text messages

## What would you do in case you face or witness any incident or repeated incidents of such behavior?

If I face sexual harassment

- Tell the harassing person to don't repeat it again
- Complain it to my superior

If I witness any sexual harassment

- Access the situation. Is it safe to intervene? Is the person being sexually harassed in danger? Will intervening make the problem better or worse? Is there someone better suited to handle this situation?
- Step in, rather than aside. If it’s safe and necessary to do so, will directly intervene in the harassment
- If direct intervention is not an option, will create a distraction to help divert the situation
- If I don’t want to get involved personally, will find an appropriate third party to intervene. This could be a manager, a supervisor, a human resource professional, another colleague, etc.
